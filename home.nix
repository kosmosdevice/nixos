{ config, pkgs, inputs, ... }:
let
  startupScript = pkgs.pkgs.writeShellScriptBin "start" ''
    ${pkgs.waybar}/bin/waybar &
    ${pkgs.swaybg}/bin/swaybg -o \* -i /etc/nixos/wallpaper/anime_room/2.png -m fill &
  '';
in
{
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
    inputs.nix-colors.homeManagerModules.default
  ];

  colorScheme = inputs.nix-colors.colorSchemes.dracula;

  programs.nixvim = {
    enable = true;
    plugins.lightline.enable = true;
    #colorschemes.gruvbox.enable = true;
    globals = {
      mapleader = " ";
    };
    keymaps = [
      {
        mode = "n";
        key = "<leader>pv";
        action = "vim.cmd.Ex";
      }
    ];
    options = {
      number = true;         
      relativenumber = true;
      guicursor = "";
      tabstop = 4;
      softtabstop = 4;
      shiftwidth = 4;
      expandtab = true;
      smartindent = true;
      wrap = false;
      swapfile = false;
      backup = false;
      hlsearch = false;
      incsearch = true;
      termguicolors = true;
      scrolloff = 8;
      signcolumn = "yes";
      updatetime = 50;
      colorcolumn = "80";
    };
  };

  home.username = "kosmos";
  home.homeDirectory = "/home/kosmos";
  home.stateVersion = "23.11"; # Please read the comment before changing.
  home.sessionVariables = {
    EDITOR = "nvim";
    GTK_THEME = "Gruvbox Dark";
  };

  dconf.settings = {
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
    };
  };

  gtk = {
    enable = true;
    theme = {
      name = "Gruvbox Dark";
      package = pkgs.gruvbox-dark-gtk;
    };
    cursorTheme = {
      name = "Numix-Cursor";
      package = pkgs.numix-cursor-theme;
    };
    iconTheme = {
      name = "Gruvbox Dark Icon";
      package = pkgs.gruvbox-dark-icons-gtk;
    };
    gtk3.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=1
      '';
    };
  };

  programs.rofi = {
    enable = true;
    font = "JetBrainsMono Nerd Font 12";
    theme = "gruvbox-dark";
  };
   
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;

    shellAliases = {
      ll = "ls -l";
      python = "python3";
      rebuild = "sudo nixos-rebuild switch --flake .#default --upgrade";
      update  = "sudo nix flake update";
      logoff = "pkill -KILL -u kosmos";
    };
    history.size = 10000;
    history.path = "${config.xdg.dataHome}/zsh/history";

    oh-my-zsh = {
      enable = true;
      plugins = [ "git" ];
      theme = "robbyrussell";
    };
  };

  programs.kitty = {
    enable = true;
    theme = "Gruvbox Dark";
    font = {
      name = "JetBrainsMono Nerd Font";
      size = 11.0;
    };
    settings = {
      enable_audio_bell = false;
      update_check_interval = 0;
      background_opacity = "0.8";
      draw_minimal_borders = "yes";
      hide_window_decorations = "yes";
    };
  };

  programs.waybar = {
    enable = true;
    settings = [
      (
        {
          layer = "top";
          position = "top";
          height = 20;
          margin-top = 0;
          margin-bottom = 5;
          spacing = 0;
          modules-left = [
            "custom/launcher"
            "hyprland/workspaces"
          ];
          modules-center = ["clock#date"];
          modules-right = [
            "pulseaudio"
            "memory"
            "cpu"
            "network"
            "battery"
            "clock"
          ];
        }
        // (import ./waybar.nix pkgs)
      )
    ];
    style = builtins.readFile ./style.css;
  };

  services.dunst = {
    enable = true;
    settings = {
      global = {
        width = "(0, 800)";
        height = 300;
        offset = "10x50";
        horizontal_padding = 10;
        frame_color = "#ebdbb2";
        font = "DejaVu Sans 16";
        format = "%s %p\\n%b";
        background = "#282828";
        foreground = "#ffffff";
        timeout = 2;
      };
      urgency_low = {};
      urgency_normal = {};
      urgency_critical = {
        frame_color = "#cc241d";
        timeout = 0;
      };
    };
  };

  programs.zathura = {
    enable = true;

    options = {
      font = "JetBrainsMono Nerd Font 12";
      adjust-open = "width";
      pages-per-row = 1;
      selection-clipboard = "clipboard";
      incremental-search = true;

      window-title-home-tilde = true;
      window-title-basename = true;
      statusbar-home-tilde = true;
      show-hidden = true;

      statusbar-h-padding = 0;
      statusbar-v-padding = 0;
      page-padding = 1;

      notification-error-bg = "#282828"; # bg
      notification-error-fg = "#fb4934"; # bright:red
      notification-warning-bg = "#282828"; # bg
      notification-warning-fg = "#fabd2f"; # bright:yellow
      notification-bg = "#282828"; # bg
      notification-fg = "#b8bb26"; # bright:green

      completion-bg = "#504945"; # bg2
      completion-fg = "#ebdbb2"; # fg
      completion-group-bg = "#3c3836"; # bg1
      completion-group-fg = "#928374"; # gray
      completion-highlight-bg = "#83a598"; # bright:blue
      completion-highlight-fg = "#504945"; # bg2

      index-bg = "#504945"; # bg2
      index-fg = "#ebdbb2"; # fg
      index-active-bg = "#83a598"; # bright:blue
      index-active-fg = "#504945"; # bg2

      inputbar-bg = "#282828"; # bg
      inputbar-fg = "#ebdbb2"; # fg

      statusbar-bg = "#504945"; # bg2
      statusbar-fg = "#ebdbb2"; # fg

      highlight-color = "#fabd2f"; # bright:yellow
      highlight-active-color = "#fe8019"; # bright:orange

      default-bg = "#282828"; # bg
      default-fg = "#ebdbb2"; # fg
      render-loading = true;
      render-loading-bg = "#282828"; # bg
      render-loading-fg = "#ebdbb2"; # fg

      recolor-lightcolor = "#282828"; # bg
      recolor-darkcolor = "#ebdbb2"; # fg
      recolor-keephue = true; # keep original color
    };

    mappings = {
      K = "zoom in";
      J = "zoom out";
      r = "reload";
      R = "rotate";
      u = "scroll half-up";
      d = "scroll half-down";
      D = "toggle_page_mode";
      i = "recolor";
      H = "navigate previous";
      L = "navigate next";
      "<Right>" = "navigate next";
      "<Left>" = "navigate previous";
      "<Down>" = "scroll down";
      "<Up>" = "scroll up";
    };
  };

  wayland.windowManager.hyprland = {
    enable = true;
    package = pkgs.hyprland;
    xwayland.enable = true;
    settings = {
      exec-once = ''${startupScript}/bin/start'';
      monitor = ",preferred,auto,1";
      input = {
        kb_layout = "se";
        follow_mouse = 1;

        touchpad = {
          natural_scroll = "yes";
        };

        sensitivity = 0;
        force_no_accel = true;
        accel_profile = "flat";
      };

      general = {
        gaps_in = 2;
	    gaps_out = 1;
	    border_size = 3;
	    layout = "dwindle";
      };

      decoration = {
        rounding = 7;
	    inactive_opacity = 0.8;
        active_opacity = 1.0;
    	fullscreen_opacity = 1.0;
    	drop_shadow = false;
    	dim_inactive = true;
    	dim_strength = 0.2;
    	shadow_range = 5;
    	shadow_render_power = 2;

        blur = {
          enabled = true;
          size = 3;
          passes = 1;
	      new_optimizations = true;
        };
      };
      
      "$mod" = "SUPER";
      bind =
	  [
          # Program executions
          "$mod, Return, exec, kitty"
          "$mod, F, exec, firefox"
          "$mod, C, killactive,"
          "$mod, M, exit," 
          "$mod, E, exec, dolphin"
          "$mod, V, exec, codium" 
          "$mod, R, exec, rofi -show drun"
          "$mod, G, exec, geary"

          # Move focus window
          "$mod, left, movefocus, l"
          "$mod, right, movefocus, r"
          "$mod, up, movefocus, u"
          "$mod, down, movefocus, d"

          # Print screen
          ",Print, exec, grim -t png ~/Bilder/Skärmbilder/-"

          # Move window with mod + LMB/RMB and dragging
          "$mod, mouse:272, movewindow"

          # Night shift bindings
          "$mod, F9, exec, wlsunset -T 6500"
          "$mod, F10, exec, pkill wlsunset"

          # Volume/media controls
          ",xf86audioraisevolume, exec, wpctl set-volume @DEFAULT_SINK@ 5%+"
          ",xf86audiolowervolume, exec, wpctl set-volume @DEFAULT_SINK@ 5%-"
          ",xf86audiomute, exec, wpctl set-mute @DEFAULT_SINK@ toggle"
          ",xf86audioplay, exec, wpctl play-pause"
          ",xf86audionext, exec, wpctl next"
          ",xf86audioprev, exec, wpctl prev"
	  ]
	++ (
	  # workspaces
	  # binds $mod + [shift +] {1..10} to [move to] workspace {1..10}
	  builtins.concatLists (builtins.genList (
	    x: let
	      ws = let
		c = (x + 1) / 10;
	      in
		builtins.toString (x + 1 - (c * 10));
	    in [
	      "$mod, ${ws}, workspace, ${toString (x + 1)}"
	      "$mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
	    ]
	  )
	10)
      );
    };
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
