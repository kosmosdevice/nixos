pkgs: {
  # LEFT
  "hyprland/workspaces" = {
    on-click = "activate";
    format = "{icon}";
    format-icons = {
      default =  "";
      "1" = "1";
      "2" = "2";
      "3" = "3";
      "4" = "4";
      "5" = "5";
      active = "󱓻";
      urgent = "󱓻";
    };
    persistent_workspaces = {
      "1" = [];
      "2" = [];
      "3" = [];
      "4" = [];
      "5" = [];
    };
    enable-bar-scroll = true;
    disable-scroll-wraparound = true;
  };

  "custom/launcher" = {
    format = " ❄ ";
    tooltip = false;
  };

  pulseaudio = {
    format = "{icon} {volume}%";
    format-bluetooth = "{volume}% {icon}";
    format-muted = "󰖁";
    format-icons = {
      headphone = "";
      hands-free = "󰋎";
      headset = "󰋎";
      default = "󰕾";
    };

    # Interaction
    scroll-step = 5;
    on-click = "wpctl set-mute @DEFAULT_SINK@ toggle";
  };

  memory = {
    interval = 1;
    format = "  {:02}%";
    max-length = 10;
    on-click = "gnome-system-monitor";
  };

  cpu = {
    interval = 1;
    format = "󰍛 {:02}%";
    max-length = 10;
    on-click = "gnome-system-monitor";
  };

  network = {
    format = "{ifname}";
    format-wifi = "  {essid}";
    format-ethernet = "󰈀 {ipaddr}";
    format-disconnected = "";
  };

  battery = {
    bat = "BAT1";
    adapter = "ACAD";
    format = "{icon}  {capacity}%";
    format-full = "{icon}  {capacity}% 󰚥";
    format-charging = "{icon}  {capacity}% 󰚥";
    format-icons = ["" "" "" "" ""];
  };

  clock = {
    interval = 5;
    format = "  {:%H:%M}";
  };

  "clock#date" = {
    interval = 5;
    format = "  {:%d/%m/%Y}";
  };
}
